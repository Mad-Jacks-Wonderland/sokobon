﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash
{
    class Box : Tile
    {
        private Level ourLevel;
        private bool onGoal;
        private Texture2D offGoalTexture;
        private Texture2D onGoalTexture;

        internal Level OurLevel { get => ourLevel; set => ourLevel = value; }
        public bool OnGoal { get => onGoal; set => onGoal = value; }
        public Texture2D OffGoalTexture { get => offGoalTexture; set => offGoalTexture = value; }
        public Texture2D OnGoalTexture { get => onGoalTexture; set => onGoalTexture = value; }

        public Box(Texture2D newOffGoalTexture, Texture2D newOnGoalTexture, Level newLevel) // constructor, they do not need a return type to work
            : base(newOffGoalTexture)
        {
            OffGoalTexture = newOffGoalTexture;
            OnGoalTexture = newOnGoalTexture;
            OurLevel = newLevel;
        }


        public bool TryPush(Vector2 direction)
        {
            //new position of the box after the push
            Vector2 newGridPos = GetTilePosition() + direction;

            Tile tileInDirection = OurLevel.GetTileAtPosition(newGridPos);

            //if target tile is a wall, we cant go there, return false
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Box)
            {
                return false;
            }



            //Ask what's on the floor in that direction
            Tile floorInDirection = OurLevel.GetFloorAtPosition(newGridPos);

            
                //if target tile is a wall, we cant go there, return false
                if (floorInDirection != null && floorInDirection is Goal)
                {
                    EnterGoal();
                }
                else if (OnGoal == true)
                {
                    ExitGoal();
                }
            

            //get info about what tile is there
            return OurLevel.TryMoveTile(this, newGridPos);


        }
        //---------------------------------------------
        private void EnterGoal()
        {
            OnGoal = true;
            texture = OnGoalTexture;
            OurLevel.EvaluteVictory();
        }
        //---------------------------------------------
        private void ExitGoal()
        {
            OnGoal = false;
            texture = OffGoalTexture;
        }
        //---------------------------------------------
        public bool GetOnGoal()
        {
            return OnGoal;
        }
    }
}
