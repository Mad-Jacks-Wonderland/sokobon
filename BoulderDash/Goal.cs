﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace BoulderDash
{
    class Goal : Tile
    {

        private Level ourLevel;

        public Goal(Texture2D newTexture, Level newLevel) // constructor, they do not need a return type to work
            : base(newTexture)
        {
            texture = newTexture;
            ourLevel = newLevel;
        }


    }
}
