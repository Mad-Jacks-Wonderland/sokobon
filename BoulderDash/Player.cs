﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BoulderDash
{
    class Player : Tile
    {
        //--------------------------
        // Data
        //-------------------------

        private Level ourLevel;
        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 0.3f;


        //--------------------------
        // Behaviour
        //-------------------------
        public Player(Texture2D newTexture, Level newLevel) // constructor, they do not need a return type to work
            : base(newTexture)
        {
            texture = newTexture;
            ourLevel = newLevel;
        }


        public override void Update(GameTime gameTime)
        {
            //add to the time since last move
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            KeyboardState keyboardState = Keyboard.GetState();

            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
            }

            if (movementInput != Vector2.Zero && timeSinceLastMove >= MOVE_COOLDOWN)
            {
                TryMove(movementInput);
                timeSinceLastMove = 0;
            }
            
        }
        //------------------
        private bool TryMove(Vector2 direction)
        {
            Vector2 newGridPos = GetTilePosition() + direction;

            //get info about what tile is there
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            //if target tile is a wall, we cant go there, return false
            if(tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }

            if (tileInDirection != null && tileInDirection is Box)
            {
                Box targetBox = tileInDirection as Box;
                bool pushSuccess = targetBox.TryPush(direction);

                if(pushSuccess == false)
                {
                    return false;
                }
            }
            // we should find out if we can move there
            // See if the move was sucessful
            bool moveResult = ourLevel.TryMoveTile(this, newGridPos);

            //return true or false depending if this is possible to move
            return moveResult;
        }
        //---------------------
   
    }
}
