﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash
{
    class Tile : Sprite
    {

        //--------------------------------
        // Data
        //--------------------------------

        private Vector2 tilePosition;
        private const int TILE_SIZE = 100;

        //--------------------------------
        // Behaviour
        //--------------------------------

        public Tile(Texture2D newTexture) // constructor, they do not need a return type to work
            : base(newTexture)
        {

        }


        public void SetTilePosition(Vector2 newTilePosition)
        {
            tilePosition = newTilePosition;
            // set our position based on tileposition
            // multiply the tile position by tile size to gain absolute position
            SetPosition(tilePosition * TILE_SIZE);
        }
        //----------------------------
        public virtual void  Update(GameTime gameTime)
        {
            //Blank - to be implemented by derived/ child classes
        }
        //-----------------------------
        public Vector2 GetTilePosition()
        {
            return tilePosition;
        }
    }
}
