﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace BoulderDash
{
    class Floor : Tile
    {
        public Floor(Texture2D newTexture, Level newLevel) // constructor, they do not need a return type to work
            : base(newTexture)
        {
            texture = newTexture;
           
        }
    }
}
