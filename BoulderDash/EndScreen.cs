﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BoulderDash
{
    class EndScreen : Screen
    {
        //------------------------
        // Data
        //------------------------

        private Text endText;
        private Text endPromt;
        private Game1 game;

        //------------------------
        // Behaviour
        //------------------------

        public EndScreen(Game1 newGame)
        {
            game = newGame;
        }


        //--------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont mainFont = content.Load<SpriteFont>("fonts/mainFont");

            endText = new Text(titleFont);
            endText.SetTextString("Game Over");
            endText.SetAlignment(Text.Alignment.CENTRE);
            endText.SetColor(Color.White);
            endText.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            endPromt = new Text(mainFont);
            endPromt.SetTextString("[Press ENTER to exit]");
            endPromt.SetAlignment(Text.Alignment.CENTRE);
            endPromt.SetColor(Color.White);
            endPromt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 300));
        }
        //------------------------

        public override void Draw(SpriteBatch spriteBatch)
        {
            endText.Draw(spriteBatch);
            endPromt.Draw(spriteBatch);
        }
        //------------------------
        public override void Update(GameTime gameTime)
        {
            //check if the player pressed Enter key
            KeyboardState keyboardState = Keyboard.GetState();

            //if player pressed enter...
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                game.ChangeScreen("title");
            }
        }
        //------------------------
    }
}
