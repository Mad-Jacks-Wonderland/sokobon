﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BoulderDash
{
    class TitleScreen : Screen
    {
        //------------------------
        // Data
        //------------------------

        private Text gameName;
        private Text startPromt;
        private Game1 game;

        //------------------------
        // Behaviour
        //------------------------

        public TitleScreen(Game1 newGame)
        {
            game = newGame;
        }
       
            
        //--------------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont mainFont = content.Load<SpriteFont>("fonts/mainFont");

            gameName = new Text(titleFont);
            gameName.SetTextString("Sokoban");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            startPromt = new Text(mainFont);
            startPromt.SetTextString("[Press ENTER to start]");
            startPromt.SetAlignment(Text.Alignment.CENTRE);
            startPromt.SetColor(Color.White);
            startPromt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 300));
        }
        //------------------------

        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.Draw(spriteBatch);
            startPromt.Draw(spriteBatch);
        }
        //------------------------
        public override void Update(GameTime gameTime)
        {
            //check if the player pressed Enter key
            KeyboardState keyboardState = Keyboard.GetState();

            //if player pressed enter...
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                game.ChangeScreen("level");
            }
        }
        //------------------------
    }
}
