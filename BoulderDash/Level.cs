﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;

namespace BoulderDash
{
    class Level : Screen
    {
        //-----------------------
        // Data
        //-----------------------

        private Tile[,] tiles;
        private Tile[,] floorTiles;
        private int currentLevel;
        private bool loadNextLevel = false;

        Texture2D wallSprite;
        Texture2D playerSprite;
        Texture2D boxSprite;
        Texture2D goalSprite;
        Texture2D floorSprite;
        Texture2D boxStoredSprite;

        

        //-----------------------
        // Data
        //-----------------------



        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            playerSprite = content.Load<Texture2D>("graphics/PlayerStatic");
            boxSprite = content.Load<Texture2D>("graphics/Box");
            goalSprite = content.Load<Texture2D>("graphics/Goal");
            floorSprite = content.Load<Texture2D>("graphics/Floor");
            boxStoredSprite = content.Load<Texture2D>("graphics/BoxStored");

            LoadLevel(1);
        }

        //---------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }

        private void CreateWall(int tileX, int tileY)
        {


            Wall wall = new Wall(wallSprite);

            wall.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = wall;

        }
        private void CreateBox(int tileX, int tileY)
        {


            Box box = new Box(boxSprite, boxStoredSprite, this);

            box.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = box;

        }

        private void CreateGoal(int tileX, int tileY)
        {


            Goal goal = new Goal(goalSprite, this);

            goal.SetTilePosition(new Vector2(tileX, tileY));
            floorTiles[tileX, tileY] = goal;

        }
        private void CreateFloor(int tileX, int tileY)
        {


            Floor floor = new Floor(floorSprite, this);

            floor.SetTilePosition(new Vector2(tileX, tileY));
            floorTiles[tileX, tileY] = floor;

        }
        private void CreatePlayer(int tileX, int tileY)
        {


            Player player = new Player(playerSprite, this);

            player.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = player;

        }
        public void LoadLevel(string fileName)
        {

            //clear any existing data
            ClearLevel();

            //Create filestream to open the file 
            //and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // before we read in the individual tiles in the level, we need to know
            //how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; //eventually will be levelWidth
            int numLines = 0; //eventually will be levelHeight

            List<string> lines = new List<string>(); //This will contain all the strings of the text in the file 
            StreamReader reader = new StreamReader(fileStream); //This will let us read each line from the file
            string line = reader.ReadLine(); //Get the first line
            lineWidth = line.Length; // assume that overall width is equal to the lenght of the first line
            while (line != null) //as long as there is a line to read
            {
                lines.Add(line); //Add the current line to the list
                if (line.Length != lineWidth)
                {
                    //this means that lines have diferrent lenghts and this will prove problematic
                    throw new Exception("Lines have diferrent widths - error occured in line " + lines.Count);
                }

                //Read the next line to get ready for another step in the loop
                line = reader.ReadLine();
            }

            //We have read in all the lines of the file into our lines list
            //We can now know how many lines there were
            numLines = lines.Count;

            //now we can set up our tile array
            
            tiles = new Tile[lineWidth, numLines];
            floorTiles = new Tile[lineWidth, numLines];

            //Loop over every tile position and check the letter
            //there and load a tile basd on the letter
            for (int y = 0; y < numLines; ++y)
            {
                for (int x = 0; x < lineWidth; ++x)
                {
                    //load each tile
                    char tileType = lines[y][x];
                    LoadTile(tileType, x, y);
                }
            }
        }


        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                case 'W':
                    CreateWall(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                case 'B':
                    CreateBox(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                case 'G':
                    CreateGoal(tileX, tileY);
                    break;

                case 'P':
                    CreatePlayer(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                case '.':
                    CreateFloor(tileX, tileY);
                    break; // do nothing

                default:
                    throw new Exception("level contains unsupported symbol " + tileType + " at line " + tileY + " and character " + tileX);
            }
        }


        private void ClearLevel()
        {

        }


        //-------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }
            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }

            if(loadNextLevel == true)
            {
                
                LoadLevel(currentLevel + 1);
            }

        }
        //---------------------
        public override void Update(GameTime gameTime)
        {
            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }
        }
        //---------------------
        public bool TryMoveTile(Tile toMove, Vector2 newPosition)
        {
            // Get the current tile position
            Vector2 CurrentTilePosition = toMove.GetTilePosition();

            //check if the new position is within bounds
            int newPosX = (int)newPosition.X;
            int newPosY = (int)newPosition.Y;
            if (newPosX >= 0
                && newPosY >= 0
                && newPosX < tiles.GetLength(0)
                && newPosY < tiles.GetLength(1))
            {
                //position is allowed

                //so, let's actually do somethign with it
                toMove.SetTilePosition(newPosition);

                //move it to correct place, and remove it from the previous place
                tiles[newPosX, newPosY] = toMove;

                tiles[(int)CurrentTilePosition.X, (int)CurrentTilePosition.Y] = null;

                return true;
            }
            else
            {
                //position is not possible
                //we could not move it, return false
                return false;
            }


        }
        //------------------
        public Tile GetTileAtPosition(Vector2 tilePos)
        {
            int PosX = (int)tilePos.X;
            int PosY = (int)tilePos.Y;
            if (PosX >= 0
                && PosY >= 0
                && PosX < tiles.GetLength(0)
                && PosY < tiles.GetLength(1))
            {
                //Yes, this cooedinate is legal
                return tiles[PosX, PosY];
            }
            else
            {
                //NO, this coordinate is not legal(out of bounds/ tile grid)
                return null;
            }
        }

        public Tile GetFloorAtPosition(Vector2 tilePos)
        {
            int PosX = (int)tilePos.X;
            int PosY = (int)tilePos.Y;
            if (PosX >= 0
                && PosY >= 0
                && PosX < floorTiles.GetLength(0)
                && PosY < floorTiles.GetLength(1))
            {
                //Yes, this cooedinate is legal
                return floorTiles[PosX, PosY];
            }
            else
            {
                //NO, this coordinate is not legal(out of bounds/ tile grid)
                return null;
            }
            //---------------------------------------



        }

        public void EvaluteVictory()
        {
            foreach (Tile tile in tiles)
            {
                if (tile != null && tile is Box)
                {
                    if ((tile as Box).GetOnGoal() == false)
                    {
                        //at least one of the boxes is not on goal, therefore we have not won yet
                        //do nothing in this case
                        return;
                    }
                }
            }


            //if we got here, we know that NO boxes are NOT on goals
            //therefore all of them are on goals
            //therefore we win
            LoadLevel(currentLevel + 1);
        }
        //-----------------------------------------
    }
}
